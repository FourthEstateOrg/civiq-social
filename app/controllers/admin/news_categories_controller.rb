module Admin
  class NewsCategoriesController < BaseController
    def index
      authorize :news_category, :index?
      @news_category = NewsCategory.new
      @news_categories = NewsCategory.order('position asc')
    end

    def create
      authorize :news_category, :create?
      @news_category = NewsCategory.new(resource_params)
      if @news_category.save
        redirect_to admin_news_categories_path
      else
        @news_categories = NewsCategory.page(params[:page])
        render :index
      end
    end

    def destroy
      authorize :news_category, :create?
      news_category = NewsCategory.find_by_id params[:id]
      news_category.destroy;
      redirect_to admin_news_categories_path;
    end

    def sort
      authorize :news_category, :sort?
      new_category_ids = params[:news_category]
      conditional = []
      new_category_ids.each_with_index do |id, index|
        conditional.concat(['WHEN', 'id', '= ', id, 'THEN', index + 1])
      end
      conditional_str = conditional.join('  ')
      sql = "UPDATE news_categories SET position = CASE ".concat(conditional_str).concat(' ').concat('END')
      ActiveRecord::Base.connection.execute(sql)
      head :ok
    end

    def show
      @news_category = NewsCategory.where(id => params[:id]).first
    end

    def resource_params
      params.require(:news_category).permit(:name, :sort_order)
    end
  end
end
