# frozen_string_literal: true
class Api::V1::NewsCategoriesController < Api::BaseController
  before_action -> { doorkeeper_authorize! :read, :'read:news_category' }
  before_action :require_user!

  DEFAULT_PAGE_SIZE = 10

  def index
    categories = NewsCategory.order('position asc')
    render json: categories, each_serializer: REST::NewsCategoriesSerializer
  end
end
