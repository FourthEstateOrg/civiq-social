require 'open-uri'
require 'digest'


class SyncFeedService < BaseService

  def initialize
    super
  end

  def call(news_source, service)
    response = download_feed(news_source.feed, news_source.etag, news_source.last_modified_at);
    if (response.present?)
      body = response.read
      hash = Digest::MD5.hexdigest body
      if news_source.modified?(hash)
        service.call(body, news_source.format, news_source.news_category)
        news_source.update(etag: response.meta['etag'], last_modified_at: response.meta['last-modified'], checksum: hash)
      end
    end
  end

  def download_feed(url, etag, last_modified_at)
    response = nil
    begin
      response = open(url, "If-Match" => etag, "If-Modified-Since" => last_modified_at, ssl_verify_mode: OpenSSL::SSL::VERIFY_NONE);
    rescue OpenURI::HTTPError => error
      if (error.io.status[0].to_i == 304)
        Rails.logger.debug "Unmodified"
      else
        raise CiviqSocial::UnexpectedResponseError, response
      end
    end
    response
  end

end


