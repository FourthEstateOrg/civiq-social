class Scheduler::NewsFeedScheduler
  include Sidekiq::Worker

  sidekiq_options unique: :until_executed, retry: 1

  def perform
    SyncNewsWorker.push_bulk(news_sources);
  end


  def news_sources
    NewsSource.active_sources.pluck(:id)
  end

end
