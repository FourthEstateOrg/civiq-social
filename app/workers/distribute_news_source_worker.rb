class DistributeNewsSourceWorker
  include Sidekiq::Worker

  sidekiq_options queue: 'push', unique: :until_executed, retry: 0


  def perform
    newsSources = NewsSource.where(:status => 'active').all.pluck(:id)
    SyncNewsWorker.push_bulk(newsSources)
  end
end
