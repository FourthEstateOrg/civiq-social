require 'rss'
require 'open-uri'


class SyncNewsWorker
  include Sidekiq::Worker

  sidekiq_options queue: 'news_feed_refresher'

  def perform(news_feed_id)
    source = NewsSource.find_by_id news_feed_id
    service= PopulateNewsService.new
    SyncFeedService.new().call(source, service)
  end
end
