class NewsSourceFilter
  attr_reader :params

  def initialize(params)
    @params = params
  end

  def results
    scope = NewsSource.order(created_at: :desc)

    params.each do |key, value|
      scope.merge!(scope_for(key, value)) if value.present?
    end
    scope
  end

  def scope_for(key, _value)
    case key.to_s
    when 'category'
      NewsSource.where(:news_category_id => _value);
    when 'status'
      NewsSource.filter_by_status(_value);
    when 'feed'
      NewsSource.filter_by_feed(_value)
    else
      raise "Unknown filter: #{key}"
    end
  end


end
