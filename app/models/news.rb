# == Schema Information
#
# Table name: news
#
#  id               :bigint(8)        not null, primary key
#  source_name      :string           not null
#  source_url       :string           not null
#  title            :string           not null
#  description      :string
#  image            :string
#  news_category_id :bigint(8)
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  meta             :jsonb            not null
#  author           :string
#  guid             :string
#  content          :string
#

class News < ApplicationRecord
  validates_presence_of :source_name
  validates_presence_of :source_url
  validates_presence_of :title

  belongs_to :news_category

  def categories
    self.meta["categories"]
  end

end
