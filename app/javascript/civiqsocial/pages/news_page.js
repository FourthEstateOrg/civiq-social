import React from 'react';
import { connect } from 'react-redux';
import ImmutablePureComponent from 'react-immutable-pure-component';
import ScrollMenu from 'react-horizontal-scrolling-menu';
import classNames from 'classnames';
import WhoToFollowPanel from '../features/ui/components/who_to_follow_panel';
import LinkFooter from '../features/ui/components/link_footer';
import PromoPanel from '../features/ui/components/promo_panel';
import UserPanel from '../features/ui/components/user_panel';
import GroupSidebarPanel from '../features/groups/sidebar_panel';
import { fetchCategories } from '../actions/news';
import { Link } from 'react-router-dom';
import { defineMessages, injectIntl } from 'react-intl';
import Carousel from 'react-bootstrap/Carousel';


const mapStateToProps = (state, { params: { category } }) => {
  return {
    categories: state.getIn(['news', 'news_categories']),
    selectedCategory: category,
  };
};

const messages = defineMessages({
  heading: { id: 'column.news', defaultMessage: 'News' },
});


export default @connect(mapStateToProps)
@injectIntl
class NewsPage extends ImmutablePureComponent {


  componentWillMount() {
    const { params: { id }, dispatch } = this.props;
    dispatch(fetchCategories());
  }

  getCaraouselItems(categories, selectedCategory, displayCategoryCount = 2) {
    const numPages = (categories.length) / displayCategoryCount;
    let categoryIdx = 0;
    const renderedCarouselItems = [];
    for (let i = 0; i < numPages; i += 1, categoryIdx += displayCategoryCount) {

      const renderedCategories = [];

      for (let j = 0; j < displayCategoryCount && (categoryIdx + j < categories.length); j += 1) {
        const category = categories[categoryIdx + j];
        renderedCategories.push(<h1 className='news-header w-25' key={category.id}>
          <Link to={`/newstab/${encodeURIComponent(category.name)}`}
                className={classNames('btn grouped', { 'active': selectedCategory === category.name })}
                key={category.id}><h1>{category.name}</h1></Link>
        </h1>);
      }
      renderedCarouselItems.push((<Carousel.Item>
        <div className='row' key={`page-${i}`}>
          {renderedCategories}
        </div>

      </Carousel.Item>));


    }
    return renderedCarouselItems;
  }

  render() {
    const { intl, children, categories, selectedCategory } = this.props;
    const MenuItem = ({category}) => {
      return <h1 className='news-header' key={category.id}>
        <Link to={`/newstab/${encodeURIComponent(category.name)}`}
              className={classNames('btn grouped', { 'active': selectedCategory === category.name })}
              key={category.id}><h1>{category.name}</h1></Link>
      </h1>;
    };


    const Menu = (categories) =>
      categories.map(category => {

        return <MenuItem category={category} key={category.name} />;
      });

    const ArrowLeft = <i className="fa fa-chevron-left" aria-hidden="true" />;

    const ArrowRight = <i className="fa fa-chevron-right" aria-hidden="true" />;

    const scrollMenuData = Menu(categories || []);

    return (
      <div className='page group'>
        <div className='page__columns'>
          <div className='columns-area__panels'>

            <div className='columns-area__panels__pane columns-area__panels__pane--left'>
              <div className='columns-area__panels__pane__inner'>
                <UserPanel/>
                <PromoPanel/>
                <LinkFooter/>
              </div>
            </div>

            <div className='columns-area__panels__main'>
              <div className='columns-area columns-area--mobile'>
                <div>
                  <div className='group-column-header'>
                    <div className='group-column-header__title'>{intl.formatMessage(messages.heading)}</div>
                    <div className='column-header__wrapper news-category'>
                      <div className='news-categories'>
                        <ScrollMenu
                          alignCenter={false}
                          data={scrollMenuData}
                          arrowLeft={ArrowLeft}
                          arrowRight={ArrowRight}
                        />

                      </div>
                    </div>
                    {/*</div>*/}
                  </div>
                </div>
                {children}
              </div>
            </div>

            <div className='columns-area__panels__pane columns-area__panels__pane--right'>
              <div className='columns-area__panels__pane__inner'>
                <GroupSidebarPanel />
                <WhoToFollowPanel />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

}
