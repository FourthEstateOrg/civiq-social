import React from 'react';
import { connect } from 'react-redux';
import { defineMessages, FormattedMessage, injectIntl } from 'react-intl';
import ImmutablePureComponent from 'react-immutable-pure-component';
import { Redirect } from 'react-router-dom';
import { expandFetchNews, fetchNews } from '../../actions/news';
import NewsCard from './card';
import ScrollableList from '../../components/scrollable_list';


const messages = defineMessages({
  heading: { id: 'column.news', defaultMessage: 'News' },
});


const mapStateToProps = (state, { params: { category } }) => {
  const news_meta = state.getIn(['news', 'news_meta']);
  return {
    categories: state.getIn(['news', 'news_categories']),
    pagination: state.getIn(['news', 'meta']),
    categoryName: category,
    news: state.getIn(['news', 'news']),
    newsMeta: news_meta,
    hasMore: news_meta && news_meta.next_page,
  };
};


export default @connect(mapStateToProps)
@injectIntl
class News extends ImmutablePureComponent {

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.categoryName !== this.props.categoryName) {
      this.props.dispatch(fetchNews(this.props.categoryName));
    }
  }

  handleLoadMore = () => {
    const { categoryName } = this.props;
    this.props.dispatch(expandFetchNews(categoryName));
  };


  render() {
    const { intl, hasMore, categories, shouldRedirect, categoryName, news, dispatch } = this.props;
    return (
      <div style={{ marginTop: '10px' }}>
        {shouldRedirect && categories && (categories.length > 0) && (<Redirect to={`/newstab/${encodeURIComponent(categories[0].name)}`} />)}
        {categoryName && !news && dispatch(fetchNews(categoryName))}
        {news ? (
          <ScrollableList
            scrollKey='categoryNews'
            hasMore={hasMore}
            onLoadMore={this.handleLoadMore}
            emptyMessage={<FormattedMessage id='empty.news' defaultMessage='No news in this category.'/>}
          >
            {news.map((aNews, idx) => <NewsCard key={`${categoryName}-${idx}`} news={aNews} category={categoryName}/>)}
          </ScrollableList>
        ) : ''}

      </div>
    );
  };

}
