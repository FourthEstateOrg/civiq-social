import React from 'react';
import ImmutablePureComponent from 'react-immutable-pure-component';
import { Link } from 'react-router-dom';

export default class NewsCard extends ImmutablePureComponent {

  render() {
    const { news, category } = this.props;
    const coverImageUrl = news.image;
    const  hasDetails = news.has_details;
    const publishDate = new Date(1000 * news.published_at).toLocaleString();
    const newsCard = (<>
      <div className='news-card-card__header'>{coverImageUrl && <img alt="" src={coverImageUrl}/>}</div>
    <div className='news-card__content'>
      <div className='news-card__title'>{news.title}</div>
      <div className='news-card__meta'>
        <strong>{news.categories}</strong>
        <div className='news-card__published_at'>Published on: {publishDate}</div>
      </div>
      <div className='news-card__description' dangerouslySetInnerHTML={{ __html: news.description }}/>
    </div>
    </>);

    const newsLink = hasDetails ? (<Link to={`/newstab/${encodeURIComponent(category)}/news/${news.id}`}>{newsCard}</Link> ): (<a href={news.source_url} target='_blank'>{newsCard}</a>);
    return (

      <div className='news-card' key={news.title}>

        {newsLink}

      </div>

    );
  }

}
