class REST::PaginationSerializer

  def self.build(objects, each_serializer)
    data = {}
    serialized_objects = []
    objects.map do |object|
      serialized_objects << each_serializer.new(object).to_h
    end
    data[objects.name.underscore.pluralize] = serialized_objects
    data[:meta] = pagination_dict(objects)
    data
  end

  def self.pagination_dict(collection)
    {
        current_page: collection.current_page,
        next_page: collection.next_page,
        prev_page: collection.prev_page,
        total_pages: collection.total_pages,
        total_count: collection.total_count,
    }
  end
end
