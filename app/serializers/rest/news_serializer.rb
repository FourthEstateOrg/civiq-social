class REST::NewsSerializer < ActiveModel::Serializer
  attributes :title, :source_name, :details_page, :id
  attributes :categories
  attributes :image
  attributes :description
  attributes :published_at
  attributes :has_details
  attributes :source_url

  def content
    CGI::unescapeHTML(object.content)
  end

  def published_at
    object.created_at.to_i
  end

  def categories
    object.categories.delete(NewsCategory.common_category)
    object.categories.join(", ")
  end

  def has_details
    object.content.present?
  end

  def details_page
     object.content.nil? ? object.source_url : "/news/#{object.id}"
  end

end
