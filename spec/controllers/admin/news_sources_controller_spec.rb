require 'rails_helper'

describe Admin::NewsSourcesController, type: :controller do

  before do
    sign_in user, scope: :user
  end

  render_views
  let(:user) { Fabricate(:user, admin: true) }

  describe 'GET #index' do
    subject { get :index, params: {active: true} }

    let!(:source) { Fabricate(:news_source) }

    it 'renders index page' do
      expect(subject).to render_template :index
    end
  end
end
