require 'rails_helper'

RSpec.describe Api::V1::NewsCategoriesController, type: :controller do
  render_views

  let(:user) { Fabricate(:user, account: Fabricate(:account, username: 'alice')) }
  let(:scopes) { 'read:news_category' }
  let(:token) { Fabricate(:accessible_access_token, resource_owner_id: user.id, scopes: scopes) }

  before { allow(controller).to receive(:doorkeeper_token) { token } }

  describe 'GET #index' do
    it 'returns response code ok' do
      tech_category = Fabricate(:news_category, name: 'tech', position: 1)
      international = Fabricate(:news_category, name: 'international', position: 2)
      get :index
      print(response.body)
      expect(response).to have_http_status(200)
    end

    context 'when there is more than one item' do
      it 'sorts by sort order' do
        Fabricate(:news_category, name: 'tech', position: 1)
        Fabricate(:news_category, name: 'international', position: 2)
        get :index, params: {limit: 1}
        response_body = JSON.parse(response.body)
        print(response_body)
        expect(response_body[0]["name"]).to eql "international"
      end

    end


    context 'with wrong scopes' do
      let(:scopes) { 'write:mutes' }

      it 'returns http forbidden' do
        get :index
        expect(response).to have_http_status(403)
      end
    end
  end
end
