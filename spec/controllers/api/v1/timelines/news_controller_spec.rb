# frozen_string_literal: true

require 'rails_helper'

describe Api::V1::Timelines::NewsController do
  render_views

  let(:user) { Fabricate(:user, account: Fabricate(:account, username: 'alice')) }
  let(:news) { Fabricate(:news) }

  before do
    allow(controller).to receive(:doorkeeper_token) { token }
  end

  context 'with a user context' do
    let(:token) { Fabricate(:accessible_access_token, resource_owner_id: user.id, scopes: 'read:news') }

    describe 'GET #index' do

      it 'returns http success when news feed is present' do
        get :index, params: {news_category_id: news.news_category.id}
        expect(response).to have_http_status(200)
      end
    end
  end

  context 'without a user context' do
    let(:token) { Fabricate(:accessible_access_token, resource_owner_id: nil, scopes: 'read:news') }

    describe 'GET #index' do
      it 'returns http unprocessable entity' do
        get :index, params: {id: news.news_category_id}

        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end
end
