require 'rails_helper'

RSpec.describe NewsCategory, type: :model do
  describe '.save' do
    let(:news_category) { Fabricate(:news_category, name: 'Tech') }

    it 'should save with name in upcase' do
      news_category.save
      expect(NewsCategory.where(:id => news_category.id).first.name).to eq "tech"
    end

    it 'should save only if name is unique' do
      news_category.save

      duplicate_category = Fabricate.build(:news_category, name: 'Tech')
      expect(duplicate_category.save).to be false
    end
  end

  describe 'delete' do
    let(:news_category) { Fabricate(:news_category, name: 'Tech') }

    it 'should delete all dependent news' do
      news = Fabricate(:news, news_category: news_category)
      news_category.destroy
      expect(News.where(id: news.id).first).to be nil
    end

    it 'should delete all depending news sources' do
      source = Fabricate(:news_source, news_category: news_category)
      news_category.destroy
      expect(NewsSource.where(id: source.id).first).to be nil
    end

  end
end
