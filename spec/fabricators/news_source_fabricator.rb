Fabricator(:news_source) do
  status 'active'
  feed 'https://www.wired.com/feed/category/science/latest/rss'
  news_category
  format 'rss'
end
