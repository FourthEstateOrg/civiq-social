# Changelog

Major changes to the Civiq platform codebase will be documented in this file.

## [3.0.0] 2020-08-28
- News Feature: pulling and displaying news from various sources

## [2.9.0] 2020-02-17
- Integration with CiviqPayments

## [1.0-beta1] 2019-10-15

- initial release
