class CreateNews < ActiveRecord::Migration[5.2]
  def change
    create_table :news do |t|
      t.string :source_name, null: false
      t.string :source_url, null: false
      t.string :title, null: false
      t.string :description
      t.string :image
      t.references :news_category
      t.timestamps
    end
  end
end
