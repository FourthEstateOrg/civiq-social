class AddEtagLastMofiedAtToNewsSources < ActiveRecord::Migration[5.2]
  def change
    add_column :news_sources, :etag, :string
    add_column :news_sources, :last_modified_at, :datetime
  end
end
