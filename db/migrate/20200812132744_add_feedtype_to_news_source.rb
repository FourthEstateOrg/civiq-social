class AddFeedtypeToNewsSource < ActiveRecord::Migration[5.2]
  def change
    add_column :news_sources, :format, :string
  end
end
