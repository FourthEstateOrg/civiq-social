class NewsItem
  attr_reader :news_title, :news_description, :content, :guid, :news_categories, :source_url, :media, :author

  def set_news_title(news_title)
    @news_title = news_title
  end

  def set_news_description(news_description)
    @news_description = news_description
  end

  def set_guid(guid)
    @guid = guid
  end

  def set_news_categories(categories = [])
    @news_categories = categories
  end

  def set_source_url(source_url)
    @source_url = source_url
  end

  def set_media(media)
    @media = media
  end

  def set_content(content)
    @content = content
  end

  def set_author(author)
    @author = author
  end

  def published_at(published_at)
    @published_at = published_at
  end

  def set_content(content)
    @content = content
  end
end
