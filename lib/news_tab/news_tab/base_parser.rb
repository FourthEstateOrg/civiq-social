module NewsTab
  class BaseParser
    attr_reader :news, :published_at

    def initialize
      @news = []
    end

    def add_news(news)
      @news.push(news)
    end

    def published_at(published_at)
      @published_at = published_at
    end
  end
end
